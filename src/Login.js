import React from 'react'
import { auth, provider } from './firebase'
import { useDispatch } from 'react-redux'
import { login } from './features/userSlice'
import { Button } from '@material-ui/core'
import './Login.css'


function Login() {
    const dispatch = useDispatch()

    const signIn = () => {
        /*
        1- auth comes from firebase package
        2- signInWithPopup(provider) to login with a signin window that pops */
        auth.signInWithPopup(provider)
            /* then(({ user }) we get the user data after a successful login */
            .then(({ user }) => {
                /* dispatch to save data to the payload of the login action from userSlice */
                dispatch(
                    login({
                        /*this specific variable names come from google */
                        displayName: user.displayName,
                        email: user.email,
                        photoUrl: user.photoURL,
                    })
                )
            })
            .catch((error) => alert(error.message))
    }

    return (
        <div className="login">
            <div className="login__container">
                <img
                    src="https://cdn.vox-cdn.com/thumbor/K-q2WRPRyxxzzPLjxHGt26swMfM=/0x0:1320x880/1200x800/filters:focal(555x335:765x545)/cdn.vox-cdn.com/uploads/chorus_image/image/67587450/newgmaillogo.0.jpg"
                    alt="gmail-rebrand-logo"
                />
                <Button
                    variant="contained"
                    color="primary"
                    onClick={signIn}
                >
                    Login
                </Button>
            </div>
        </div>
    )
}

export default Login
