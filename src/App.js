import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { selectSendMessageIsOpen } from './features/mailSlice.js'
import { selectUser } from './features/userSlice.js';
import { auth } from './firebase'
import { login } from './features/userSlice'
import './App.css';
import Header from './Header.js'
import Sidebar from './Sidebar.js'
import Mail from './Mail.js'
import EmailList from './EmailList.js'
import SendMail from './SendMail.js'
import Login from './Login.js'

function App() {

  /* We use the selector from the mailSlice */
  const sendMessageIsOpen = useSelector(selectSendMessageIsOpen)
  const user = useSelector(selectUser)
  const dispatch = useDispatch()

  useEffect(() => {
    /* uth.onAuthStateChanged((user) checks the changes on the user variable, if exists then it logs in */
    auth.onAuthStateChanged((user) => {
      if (user) {
        // The user is logged in
        dispatch(
          login({
            displayName: user.displayName,
            email: user.email,
            photoUrl: user.photoURL,
          })
        )
      }
    })
  }, [])

  return (
    <Router>
      {!user ? (
        <Login />
      ) : (
        <div className="app">
          <Header />

          <div className="app__body">
            <Sidebar />

            <Switch>
              <Route path="/mail">
                <Mail />
              </Route>

              <Route path="/">
                <EmailList />
              </Route>
            </Switch>
          </div>

          {/* Renders the SendMail component when the selector is true */}
          {sendMessageIsOpen && <SendMail />}
        </div>
      )}
    </Router>
  );
}

export default App;
