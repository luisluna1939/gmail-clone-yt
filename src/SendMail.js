import React from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { db } from './firebase'
import firebase from "firebase/compat/app"
import { closeSendMessage } from './features/mailSlice'
import { Button } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import './SendMail.css'

function SendMail() {

    //Usage of useForm
    const { register, handleSubmit, watch, errors } = useForm()
    const dispatch = useDispatch()

    //Function to handle the data from the form
    const onSubmit = (formData) => {
        console.log(formData)
        //This saves a document in the collection from firestore db
        db.collection('emails').add(
            {
                to: formData.to,
                subject: formData.subject,
                message: formData.message,
                timestamp: firebase.firestore.FieldValue.serverTimestamp() //time from the server
            })

        //To close the form after submit
        dispatch(closeSendMessage())
    }

    return (
        <div className="sendMail">
            <div className="sendMail__header">
                <h3>New Message</h3>
                {/*Close the form using dispatch */}
                <CloseIcon onClick={() => dispatch(closeSendMessage())}
                    className="sendMail__close" />
            </div>

            <form onSubmit={handleSubmit(onSubmit)}>
                <input
                    name='to'
                    placeholder="To"
                    type="email"
                    ref={register({ required: true })} //we say a field is required with this function from useForm()
                />

                {/* If there's a error on the to input then renders the p tag */}
                {errors.to && <p className="sendMail__error">To is required</p>}

                <input
                    name='subject'
                    placeholder="Subject"
                    type="text"
                    ref={register({ required: true })}
                />

                {errors.subject && <p className="sendMail__error">Subject is required</p>}

                <input
                    name='message'
                    placeholder="Message..."
                    type="text"
                    className="sendMail__message"
                    ref={register({ required: true })}
                />

                {errors.message && <p className="sendMail__error">Message is required</p>}

                <div className="sendMail__options">
                    <Button
                        className="sendMail__send"
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Send
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default SendMail
