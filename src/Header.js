import React from 'react'
import './Header.css'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import AppsIcon from '@material-ui/icons/Apps'
import NotificationsIcon from '@material-ui/icons/Notifications'
import { Avatar, IconButton } from '@material-ui/core';
import { useSelector } from 'react-redux'
import { selectUser } from './features/userSlice'
import { useDispatch } from 'react-redux'
import { logout } from './features/userSlice'
import { auth } from './firebase'

function Header() {

    const user = useSelector(selectUser)
    const dispatch = useDispatch()

    const signOut = () => {
        /* we signout from firebase with auth.signOut() then we let the user variable on null with logout action  */
        auth.signOut().then(() => {
            dispatch(logout())
        })
    }

    return (
        <div className="header">
            <div className="header__left">
                <IconButton> {/** Turn an icon into a button */}
                    <MenuIcon />
                </IconButton>

                <img
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfb1atodW4VMnvc3Gh0vV7yNnfWvETH6W8mR8Dnk6COTFn73Pqf2bX0djgK1-XBfy1Yg&usqp=CAU"
                    alt="gmail-logo"
                />
            </div>

            <div className="header__middle">
                <SearchIcon />
                <input placeholder="Search mail" type="text" />
                <ArrowDropDownIcon className="header__inputCaret" />
            </div>

            <div className="header__right">
                <IconButton>
                    <AppsIcon />
                </IconButton>

                <IconButton>
                    <NotificationsIcon />
                </IconButton>

                <Avatar onClick={signOut} className="header__avatar" src={user?.photoUrl} />
            </div>
        </div>
    )
}

export default Header
